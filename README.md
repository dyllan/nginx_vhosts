# nginx_vhosts

Create and delete virtualhosts for Nginx on Centos.

# usage

# nginx.txt & php-fpm.txt

These are example and somewhat default .conf files for an nginx and php-fpm virtualhosts.
Edit them as required **preferably** after using the program making sure to keep the variable placeholders in tact if you do happen modify the .txt files directly otherwise the program will fail.

# txt file variable placeholders
_virtual_host, public_dir_

# vhostadd

```
python3 vhostadd --vhost example.com --public public --restart
```

# vhostadd options
```
-v --vhost      Virtualhost name
-w --www        Create virtualhost www alias
-p --public     Public directory name
-r --restart    Restart services
```


# vhostdel

```
python3 vhostdel --vhost example.com --backup --restart
```

# vhostdel options 
```
-v --vhost      Virtualhost name
-b --backup     Backup virtualhost before deleting
-r --restart    Restart services to finish deleting user
-f --force      Force user deletion without restarting services
```
