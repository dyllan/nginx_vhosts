server {
    #gzip on;
    #gzip_vary on;
    #gzip_types      text/plain text/css text/xml text/x-component text/js text/javascript application/javascript application/x-javascript application/json application/xml application/xml+rss font/truetype font/opentype;
    #gzip_proxied    no-cache no-store private expired auth;
    #gzip_min_length 1000;
    #gzip_buffers    16 8k;

    root /home/virtual_host/public_html/public_dir;
    index index.php index.html index.htm;

    server_name virtual_host;

    access_log /var/log/nginx/virtual_host-access.log;
    error_log /var/log/nginx/virtual_host-error.log;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
        #client_max_body_size 500M;
    }

    location ~ \.php$ {
        try_files $uri = 404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/run/php-fpm/virtual_host.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
        #client_max_body_size 500M;
        #client_body_timeout 120;
    }
}