#!/usr/bin/env python3
from argparse import ArgumentParser
from subprocess import getstatusoutput
from os import path, strerror
from errno import ENOENT
from string import ascii_letters, digits
from datetime import datetime
from utils import check, restart, stop, start, log_ok, log_error, log_warning


class VirtualHostDel:
    """
    A class to represent a virtualhost to be deleted.

    ...

    Attributes
    ----------
    vhost : str
        name of the virtualhost to be deleted
    backup : bool
        should a backup of the virtualhost be performed
    restart : bool
        stop services to issue userdel command, then start services
    force : bool
        force usage of userdel to prevent restarting services

    Methods
    -------
    del_user():
        Deletes the configuration fils and home directory of the virtualhost.
    perform_backup():
        Creates a tar.gz file of the configuration files and home directory of the
        virtualhost before deleting the virtualhost.
    """

    def __init__(self, vhost: str, backup: bool, restart: bool, force: bool) -> None:
        """
        Constructs all the necessary attributes for the virtualhost object.

        Parameters
        ----------
            vhost : str
                name of the virtualhost to be deleted
            backup : bool
                should a backup of the virtualhost be performed
            restart : bool
                stop services to issue userdel command, then start services
            force : bool
                force usage of userdel to prevent restarting services
        """
        self.vhost = vhost
        self.backup = backup
        self.restart = restart
        self.force = force
        self.nginx_dir = "/etc/nginx/conf.d/"
        self.php_fpm_dir = "/etc/php-fpm.d/"

        check()

    def del_user(self):
        """
        Deletes the virtualhost as a user on the system along with
        configuration files.
        """
        try:
            getstatusoutput(f"rm -f {self.nginx_dir + self.vhost}.conf")
            log_ok("nginx conf file deleted")
        except (PermissionError, FileNotFoundError):
            log_error("unable to delete nginx conf file")

        try:
            getstatusoutput(f"rm -f {self.php_fpm_dir + self.vhost}.conf")
            log_ok("php-fpm conf file deleted")
        except (PermissionError, FileNotFoundError):
            log_error("unable to delete php-fpm conf file")

        if self.force:
            status = getstatusoutput(f"userdel -rf {self.vhost}")
            if status[0] == 0:
                log_ok(self.vhost + " virtualhost deleted")
            else:
                log_error("unable to force delete the virtualhost")
                raise Exception("Error while attempting userdel -rf")
        else:
            stop()
            status = getstatusoutput(f"userdel -r {self.vhost}")
            if status[0] == 0:
                log_ok(self.vhost + " virtualhost deleted")
                start()
            else:
                log_error("unable to delete the virtualhost")
                raise Exception("Error while attempting userdel -r")

    def perform_backup(self):
        """
        Deletes the virtualhost as a user on the system along with
        configuration files.
        """
        if self.backup:
            if path.isfile(f"/tmp/{self.vhost}.backup.tar.gz"):
                try:
                    timestamp = datetime.now()
                    status = getstatusoutput(
                        f"mv /tmp/{self.vhost}.backup.tar.gz /tmp/{self.vhost}-{timestamp.strftime('%H-%M-%S')}.backup.tar.gz")
                    if status[0] == 0:
                        log_ok("Found older backup, moved and added timestamp")
                    else:
                        raise OSError(strerror(ENOENT))
                except OSError as e:
                    log_warning("unable to move older backup")
                    log_warning(e)
                    raise

            try:
                if path.isfile(f"{self.nginx_dir + self.vhost}.conf"):
                    status = getstatusoutput(
                        f"tar --append --file=/tmp/{self.vhost}.backup.tar {self.nginx_dir + self.vhost}.conf")
                    if status[0] == 0:
                        log_ok("nginx conf file added to backup")
                else:
                    raise OSError(strerror(ENOENT))
            except OSError as e:
                log_warning("unable to add nginx conf file to backup")
                log_warning(e)

            if path.isfile(f"{self.php_fpm_dir + self.vhost}.conf"):
                try:
                    status = getstatusoutput(
                        f"tar --append --file=/tmp/{self.vhost}.backup.tar {self.php_fpm_dir + self.vhost}.conf")
                    if status[0] == 0:
                        log_ok("php-fpm conf file added to backup")
                except Exception:
                    log_error("unable to add php-fpm conf file to backup")

            if path.isdir(f"/home/{vhost}"):
                try:
                    status = getstatusoutput(
                        f"tar --append --file=/tmp/{self.vhost}.backup.tar /home/{self.vhost}")
                    if status[0] == 0:
                        log_ok(f"backing up to /tmp/{self.vhost}.backup.tar")
                        try:
                            status = getstatusoutput(
                                f"gzip /tmp/{self.vhost}.backup.tar")
                            if status[0] == 0:
                                log_ok(
                                    f"backup compressed to /tmp/{self.vhost}.backup.tar.gz")
                        except Exception:
                            log_error("unable to compress backup")
                        self.del_user()
                except Exception:
                    log_error("unable to add virtualhost home dir to backup")
            else:
                log_error("virtulhost does not exist")
        else:
            self.del_user()


if __name__ == "__main__":
    parser = ArgumentParser(prog="vhost",
                            description="Virtualhost Management")
    force_restart = parser.add_mutually_exclusive_group(required=True)
    parser.add_argument("-v", "--vhost", type=str, required=True,
                              help="Virtualhost name")
    parser.add_argument("-b", "--backup", action="store_true", required=False,
                              help="Backup virtualhost before deleting")
    force_restart.add_argument("-r", "--restart", action="store_true", required=False,
                               help="Restart services to finish deleting user")
    force_restart.add_argument("-f", "--force", action="store_true", required=False,
                               help="Force user deletion without restarting services")

    args = parser.parse_args()
    vhost = args.vhost.strip().lower()
    backup = args.backup
    restart = args.restart
    force = args.force

    for char in vhost:
        if char not in (ascii_letters + digits + "-._"):
            log_error("invalid char in input")
            raise Exception("Illegal characters in input")

    VirtualHostDel(vhost, backup, restart, force).perform_backup()
