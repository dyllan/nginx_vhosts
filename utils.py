from os import system
from subprocess import getstatusoutput
try:
    from termcolor import colored
except ImportError:
    def colored(text: str, color: str) -> str:
        return text

NGINX = "nginx.service"
PHP_FPM = "php-fpm.service"


def log_ok(*args, **kwargs):
    """Set green colour for OK response"""
    print(colored("[OK]", "green"), *args, **kwargs)


def log_warning(*args, **kwargs):
    """Set green colour for OK response"""
    print(colored("[Warning]", "yellow"), *args, **kwargs)


def log_error(*args, **kwargs):
    """Set red colour for ERROR response"""
    print(colored("[ERROR]", "red"), *args, **kwargs)


def check():
    """Checks the system services to see if nginx and php-fpm are active."""
    if system(f"systemctl is-active --quiet {NGINX}") == 0 \
            and system(f"systemctl is-active --quiet {PHP_FPM}") == 0:
        log_ok("nginx & php-fpm installed and running...")
    else:
        log_error("Please ensure that " + NGINX + " and " +
                  PHP_FPM + " are installed and active.")
        raise Exception("Services missing or not active.")


def restart():
    """Restarts the nginx and php-fpm service."""
    status = getstatusoutput(
        f"systemctl restart {NGINX} {PHP_FPM}")
    if status[0] == 0:
        log_ok(NGINX + " & " + PHP_FPM + " restarted...")
    else:
        log_error("Unable to restart " + NGINX +
                  " or " + PHP_FPM + " service.")
        raise Exception("Unable to restart system services.")


def stop():
    """Stops the nginx and php-fpm service."""
    status = getstatusoutput(f"systemctl stop {NGINX} {PHP_FPM}")
    if status[0] == 0:
        log_ok("stopping " + NGINX + " & " + PHP_FPM)
    else:
        log_error("Unable to stop " + NGINX +
                  " or " + PHP_FPM + " service.")
        raise Exception("Unable to stop system services.")


def start():
    """Starts the nginx and php-fpm service."""
    status = getstatusoutput(
        f"systemctl start {NGINX} {PHP_FPM}")
    if status[0] == 0:
        log_ok("starting " + NGINX + " & " + PHP_FPM)
    else:
        log_error("Unable to start " + NGINX +
                  " or " + PHP_FPM + " service.")
        raise Exception("Unable to start system services.")
