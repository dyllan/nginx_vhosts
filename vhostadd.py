#!/usr/bin/env python3
from argparse import ArgumentParser
from os import makedirs
from subprocess import getstatusoutput
from string import ascii_letters, digits
from utils import check, restart, log_ok, log_error


class VirtualHostAdd:
    """
    A class to represent a virtualhost to be created.

    ...

    Attributes
    ----------
    vhost : str
        name of the virtualhost to be created
    public_dir : str
        name of the public web directory

    Methods
    -------
    add_user():
        Creates the virtualhost as a user on the system.
    create_configuration():
        Creates the necessary configuration for nginx and php-fpm by reading 
        the contents of the .conf files and replacing the relevant fields 
        with our virtual_host and public_dir parameters.
    create_public_directory():
        Creates a directory in the public_html folder with name specified as
        public_dir parameter and creates a index.html file placeholder.
    set_permissions():
        Sets the required user and access permissions on the directories and
        files of the new virtualhost.
    """

    def __init__(self, vhost: str, www: bool, public_dir: str, service_restart: bool) -> None:
        """
        Constructs all the necessary attributes for the virtualhost object.

        Parameters
        ----------
            vhost : str
                name of the virtualhost to be created
            public_dir : str
                name of the public web directory
        """
        self.vhost = vhost
        self.www = www
        self.public_dir = public_dir
        self.service_restart = service_restart
        self.nginx_dir = "/etc/nginx/conf.d/"
        self.php_fpm_dir = "/etc/php-fpm.d/"

        check()

    def add_user(self):
        """
        Creates the virtualhost as a user on the system.
        """
        status = getstatusoutput(f"useradd -m {self.vhost}")
        if status[0] == 0:
            log_ok(self.vhost + " virtualhost created...")
            self.create_configuration()
        else:
            log_error("unable to create the virtualhost as a user.")
            raise Exception("Error while attempting to adduser.")

    def create_configuration(self):
        """
        Creates the necessary configuration for nginx and php-fpm by reading 
        the contents of the .conf files and replacing the relevant fields 
        with our virtual_host and public_dir parameters.
        """
        try:
            with open("nginx.txt", "r") as file:
                nginx_conf = file.read()
                if www is True:
                    nginx_conf = nginx_conf.replace(
                        "server_name virtual_host;", "server_name virtual_host www.virtual_host;")
                # Specific nginx config necessary for Laravel when using dist as public directory.
                if public_dir == "dist":
                    nginx_conf = nginx_conf.replace(
                        "try_files $uri $uri/ /index.php?$query_string;", "try_files $uri $uri/ /index.html;")
                nginx_conf = nginx_conf.replace(
                    "virtual_host", self.vhost)
                nginx_conf = nginx_conf.replace("public_dir", self.public_dir)
            with open("php-fpm.txt", "r") as file:
                php_fpm_conf = file.read()
                php_fpm_conf = php_fpm_conf.replace(
                    "virtual_host", self.vhost)
        except FileNotFoundError as e:
            log_error("unable to read conf files.")
            raise FileNotFoundError(e) from None

        try:
            with open(self.nginx_dir + self.vhost + ".conf", "x") as file:
                file.write(nginx_conf)
            with open(self.php_fpm_dir + self.vhost + ".conf", "x") as file:
                file.write(php_fpm_conf)
            log_ok("conf files created...")
            self.create_public_directory()
        except (PermissionError, FileExistsError) as e:
            log_error("unable to create conf files.")
            raise Exception(e) from None

    def create_public_directory(self):
        """
        Creates a directory in the public_html folder with name specified as
        public_dir parameter and creates a index.html file placeholder.
        """
        home_dir = (
            f"/home/{self.vhost}/public_html/{self.public_dir}/")
        try:
            makedirs(home_dir)
            with open(home_dir + "index.html", "x") as file:
                file.write("Welcome to " + self.vhost)
            log_ok(self.public_dir + " dir created...")
            self.set_permissions()
        except (PermissionError, FileExistsError) as e:
            log_error("unable to create public dir.")
            raise Exception(e) from None

    def set_permissions(self):
        """
        Sets the required user and access permissions on the directories and
        files of the new virtualhost.
        """
        status_chmod = getstatusoutput(
            f"chmod -R 770 /home/{self.vhost}")
        status_chown = getstatusoutput(
            f"chown -R {self.vhost}:nginx /home/{self.vhost}")
        if status_chmod[0] == 0 and status_chown[0] == 0:
            log_ok("dir permissions set...")
            if self.service_restart is True:
                restart()
            else:
                print("Restart flag not set, please restart services manually.")
        else:
            log_error("Unable to set dir permissions.")
            raise Exception("Error setting permissions.")


if __name__ == "__main__":
    parser = ArgumentParser(prog="vhost",
                            description="Virtualhost Management")
    parser.add_argument("-v", "--vhost", type=str, required=True,
                              help="Virtualhost name")
    parser.add_argument('-w', action='store_true',
                        required=False, help="Creates virtualhost www alias")
    parser.add_argument("-p", "--public", required=True,
                              help="Public directory name")
    parser.add_argument("-r", "--restart", action="store_true", required=False,
                              help="Restart services")

    args = parser.parse_args()
    vhost = args.vhost.strip().lower()
    www = args.w
    public_dir = args.public.strip().lower()
    service_restart = args.restart

    for char in (vhost + public_dir):
        if char not in (ascii_letters + digits + "-._"):
            log_error("invalid char in input.")
            raise Exception("Illegal characters in input.")

    VirtualHostAdd(vhost, www, public_dir, service_restart).add_user()
